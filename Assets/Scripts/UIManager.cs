using NatSuite.Recorders;
using NatSuite.Recorders.Clocks;
using NatSuite.Recorders.Inputs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager I;

    public SpriteRenderer mainImage;
    public AudioSource audioSource;
    public GameObject genericFrame;
    public GameObject genericOverlay;
    public GameObject stickerUIObject;
    public Transform stickerUIObjectParent;
    public GameObject watermark;
    public GameObject progressBar;

    public List<SimpleEffects> allEffects;

    public static bool IsEditMode;
    public static bool IsRecording;

    public static Sprite UserImage;
    public static AudioClip UserAudioClip;
    public static SimpleEffects UserEffect;
    public static float AveragedVolume;

    private bool IsImageZoom;
    private bool IsImageBlink;
    private Material mainImageMat;
    private Text progressText;


    private void Awake()
    {
        I = this;
    }

    private void Start()
    {
        IsEditMode = true;
        mainImageMat = mainImage.material;
        progressText = progressBar.GetComponentInChildren<Text>();

        foreach (var simpleEffect in allEffects)
        {
            foreach (var effect in simpleEffect.particles)
            {
                var main = effect.main;
                main.prewarm = true;
            }
            simpleEffect.gameObject.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        AveragedVolume = GetAveragedVolume();

        if (IsRecording)
        {
            progressText.text = (audioSource.time / audioSource.clip.length * 100).ToString("0") + " %";
        }
    }

    private void LateUpdate()
    {
        if (IsImageZoom)
        {
            float scale = Mathf.Clamp(Mathf.Abs(AveragedVolume / 256) + 1, 1.35f, 1.55f);

            Vector3 targetScale = new Vector3(scale, scale, 1);

            mainImage.transform.localScale = Vector3.Lerp(mainImage.transform.localScale, targetScale, 10 * Time.deltaTime);
        }

        if (IsImageBlink)
        {
            //vivek changes
            float blinkVal = Mathf.Clamp01(AveragedVolume / 100);
            mainImageMat.SetFloat("_EffectAmount", blinkVal);
        }

    }

    public void SetImageAndMusic(string imgPath, string mp3Path, bool randomEffect)
    {
        StopCoroutine("SetImageAndMusicNumerator");
        StartCoroutine(SetImageAndMusicNumerator(imgPath, mp3Path, randomEffect));
    }

    public void SetEffect(int index)
    {
        if (UserEffect != null)
        {
            UserEffect.gameObject.SetActive(false);
        }
        UserEffect = allEffects[index];
        UserEffect.gameObject.SetActive(true);
    }

    public void SetTheme(int themeVal)
    {
        Vector3 defaultMainImageScale = new Vector3(1.35f, 1.35f, 1);

        switch (themeVal)
        {
            case 0:
                // blink zoom
                IsImageZoom = true;
                IsImageBlink = true;
                break;

            case 1:
                // blink
                IsImageZoom = false;
                IsImageBlink = true;
                mainImage.transform.localScale = defaultMainImageScale;
                break;

            case 2:
                // bw zoom
                IsImageZoom = true;
                IsImageBlink = false;
                mainImage.transform.localScale = defaultMainImageScale;
                mainImageMat.SetFloat("_EffectAmount", 1);
                break;

            case 3:
                // bw
                IsImageZoom = false;
                IsImageBlink = false;
                mainImage.transform.localScale = defaultMainImageScale;
                mainImageMat.SetFloat("_EffectAmount", 1);
                break;

            case 4:
                // solo zoom
                IsImageZoom = true;
                IsImageBlink = false;
                mainImage.transform.localScale = defaultMainImageScale;
                mainImageMat.SetFloat("_EffectAmount", 0);
                break;

            case 5:
                // solo
                IsImageZoom = false;
                IsImageBlink = false;
                mainImage.transform.localScale = defaultMainImageScale;
                mainImageMat.SetFloat("_EffectAmount", 0);
                break;
        }
    }

    public void SetFrame(string framePath)
    {
        genericFrame.GetComponent<SpriteRenderer>().sprite = GetImageSpriteAtPath(framePath);

        //genericFrame.transform.localScale = Vector3.one;
        genericFrame.SetActive(true);
        //Debug.Log("genericFrame : transform:::::" + genericFrame.transform.localScale);
        Debug.Log("genericFrame sizepixel:::::::" + genericFrame.GetComponent<SpriteRenderer>().sprite.rect);
        //Debug.Log("genericFrame : size_after:::::" + genericFrame.transform.localScale);
    }

    public void SetOverlay(string overlayPath)
    {
        genericOverlay.GetComponent<SpriteRenderer>().sprite = GetImageSpriteAtPath(overlayPath);
        genericOverlay.SetActive(true);
    }

    public void SetSticker(string stickerPath)
    {
        Sprite stickerSprite = GetImageSpriteAtPath(stickerPath);
        GameObject sticker = Instantiate(stickerUIObject, stickerUIObjectParent);
        sticker.GetComponent<StickerDrag>().SetImage(stickerSprite);
    }

    public void CreateAndSaveVideo(bool isWatermarkShow)
    {
        IsEditMode = false;
        StickerDrag.DisableAllSticker();
        watermark.SetActive(isWatermarkShow);
        progressText.text = "0 %";
        progressBar.SetActive(true);
        audioSource.Stop();
        audioSource.loop = false;
        StartRecording();
    }

    private IEnumerator SetImageAndMusicNumerator(string imgPath, string mp3Path, bool randomEffect)
    {
        if (!string.IsNullOrEmpty(imgPath))
        {
            Texture2D texture2D = NativeGallery.LoadImageAtPath(imgPath, 1024, true, false);

            texture2D = duplicateTexture(texture2D);
            texture2D = TextureTools.ResampleAndCrop(texture2D,540,960);
            Rect rect = new Rect(0, 0, texture2D.width, texture2D.height);
            Vector2 pivot = new Vector2(0.5f, 0.5f);
            UserImage = Sprite.Create(texture2D, rect, pivot);
            mainImage.sprite = UserImage;
            //Debug.Log("mainImage.bounds.size:::::::" + mainImage.bounds.size);
            //Debug.Log("mainImage.size:::::::" + mainImage.transform.localScale);
            //genericFrame.transform.localScale = mainImage.transform.localScale;
            //Debug.Log("genericFrame.size:::::::" + genericFrame.GetComponent<SpriteRenderer>().size);
            yield return new WaitForEndOfFrame();
        }

        if (!string.IsNullOrEmpty(mp3Path))
        {
            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(mp3Path, AudioType.MPEG))
            {
                yield return www.SendWebRequest();

                if (www.isHttpError || www.isNetworkError)
                {
                    Debug.Log("Audio Error:- " + www.error);
                }
                else
                {
                    UserAudioClip = DownloadHandlerAudioClip.GetContent(www);
                    audioSource.clip = UserAudioClip;
                    audioSource.loop = true;
                    audioSource.Play();
                }
            }
        }

        if (randomEffect)
        {
            UserEffect = allEffects[Random.Range(0, allEffects.Count)];
            UserEffect.gameObject.SetActive(true);
        }

        TalkToAndroid.I.ShowSplashScreen();
    }

    private Sprite GetImageSpriteAtPath(string imgPath)
    {
        Texture2D texture2D = NativeGallery.LoadImageAtPath(imgPath, -1, true, false);
        Rect rect = new Rect(0, 0, texture2D.width, texture2D.height);
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        Sprite sprite = Sprite.Create(texture2D, rect, pivot);

        return sprite;
    }

    private float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        audioSource.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a;
    }

    private IMediaRecorder recorder;
    private CameraInput cameraInput;
    private AudioInput audioInput;

    public void StartRecording()
    {
        IsRecording = true;
        var frameRate = 60;
        var sampleRate = AudioSettings.outputSampleRate;
        var channelCount = (int)AudioSettings.speakerMode;
        var clock = new RealtimeClock();
        recorder = new MP4Recorder(720, 1280, frameRate, sampleRate, channelCount);

        cameraInput = new CameraInput(recorder, clock, Camera.main);
        audioInput = new AudioInput(recorder, clock, audioSource);
        audioSource.Play();
        Invoke("StopRecording", audioSource.clip.length);
    }

    public async void StopRecording()
    {
        audioInput.Dispose();
        cameraInput.Dispose();
        var path = await recorder.FinishWriting();
        IsRecording = false;
        progressBar.SetActive(false);
        TalkToAndroid.I.FinishVideo(path);
    }




    Texture2D duplicateTexture(Texture2D source)
    {
        RenderTexture renderTex = RenderTexture.GetTemporary(
                    source.width,
                    source.height,
                    0,
                    RenderTextureFormat.Default,
                    RenderTextureReadWrite.Linear);

        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }

    Texture2D RoundCrop(Texture2D sourceTexture)
    {
        int width = sourceTexture.width;
        int height = sourceTexture.height;
        float radius = (width < height) ? (width / 2f) : (height / 2f);
        float centerX = width / 2f;
        float centerY = height / 2f;
        Vector2 centerVector = new Vector2(centerX, centerY);

        // pixels are laid out left to right, bottom to top (i.e. row after row)
        Color[] colorArray = sourceTexture.GetPixels(0, 0, width, height);
        Color[] croppedColorArray = new Color[width * height];
        //Texture2D croppedTexture_change = new Texture2D((int)radius, (int)radius);
        for (int row = 0; row < height; row++)
        {
            for (int column = 0; column < width; column++)
            {
                int colorIndex = (row * width) + column;
                float pointDistance = Vector2.Distance(new Vector2(column, row), centerVector);
                if (pointDistance < radius)
                {
                    croppedColorArray[colorIndex] = colorArray[colorIndex];
                    //croppedTexture_change.SetPixel(column,row, colorArray[colorIndex]);
                }
                else
                {
                    //sourceTexture.SetPixel(row, column,);
                    //croppedColorArray[colorIndex] = ;
                    croppedColorArray[colorIndex] = Color.clear;
                }
            }
        }

        Texture2D croppedTexture = new Texture2D(width, height);
        croppedTexture.SetPixels(croppedColorArray);
        croppedTexture.Apply();
        //croppedTexture_change.Apply();
        croppedTexture = TextureTools.ResampleAndCrop(croppedTexture, (int)radius, (int)radius);
        croppedTexture.Apply();
        return croppedTexture;
    }


}
public class TextureTools
{
    public static Texture2D ResampleAndCrop(Texture2D source, int targetWidth, int targetHeight)
    {
        int sourceWidth = source.width;
        int sourceHeight = source.height;
        float sourceAspect = (float)sourceWidth / sourceHeight;
        float targetAspect = (float)targetWidth / targetHeight;
        int xOffset = 0;
        int yOffset = 0;
        float factor = 1;
        if (sourceAspect > targetAspect)
        { // crop width
            factor = (float)targetHeight / sourceHeight;
            xOffset = (int)((sourceWidth - sourceHeight * targetAspect) * 0.5f);
        }
        else
        { // crop height
            factor = (float)targetWidth / sourceWidth;
            yOffset = (int)((sourceHeight - sourceWidth / targetAspect) * 0.5f);
        }
        Color32[] data = source.GetPixels32();
        Color32[] data2 = new Color32[targetWidth * targetHeight];
        for (int y = 0; y < targetHeight; y++)
        {
            for (int x = 0; x < targetWidth; x++)
            {
                var p = new Vector2(Mathf.Clamp(xOffset + x / factor, 0, sourceWidth - 1), Mathf.Clamp(yOffset + y / factor, 0, sourceHeight - 1));
                // bilinear filtering
                var c11 = data[Mathf.FloorToInt(p.x) + sourceWidth * (Mathf.FloorToInt(p.y))];
                var c12 = data[Mathf.FloorToInt(p.x) + sourceWidth * (Mathf.CeilToInt(p.y))];
                var c21 = data[Mathf.CeilToInt(p.x) + sourceWidth * (Mathf.FloorToInt(p.y))];
                var c22 = data[Mathf.CeilToInt(p.x) + sourceWidth * (Mathf.CeilToInt(p.y))];
                var f = new Vector2(Mathf.Repeat(p.x, 1f), Mathf.Repeat(p.y, 1f));
                data2[x + y * targetWidth] = Color.Lerp(Color.Lerp(c11, c12, p.y), Color.Lerp(c21, c22, p.y), p.x);
            }
        }

        var tex = new Texture2D(targetWidth, targetHeight);
        tex.SetPixels32(data2);
        tex.Apply(true);
        return tex;
    }
}
