using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class SpectrumAnalyzer : MonoBehaviour
{
	public GameObject mainImage;

	public UIManager videoCreationManager;

	public List<ParticleSystem> particles;

	public bool shouldChangeColorOnBeat;

	public float imageScaleFactor;

	public float particleSpeedFactor;

	public static bool isPlayingAudio;

	public List<Animator> animateObjectOnBeat;

	public Animator frameAnimator;

	private AudioSource source;

	private float lastUpdate;

	private float baseSize;

	private float newValue;

	private float oldScale;

	private float[] spectrum;

	public ThemeCategory currentThemeFilter;

	public static float beatEffect;

	private float particleBumpOnSongValue;

	private void Start()
	{
	}

	private void FixedUpdate()
	{
	}

	private void SetEffectOnImage(float effectFactor)
	{
	}

	public void StopAllParticles()
	{
	}

	public void SetNewImageSize(float newSize)
	{
	}

	public void ChangeParticles(List<ParticleSystem> newParticles, [Optional] List<Animator> objectsToDisplay)
	{
	}

	public void ChangeThemeFilter(ThemeCategory newTheme)
	{
	}
}
