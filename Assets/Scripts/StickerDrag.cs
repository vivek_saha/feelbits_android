using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Lean.Touch;

public class StickerDrag : MonoBehaviour, IDragHandler, IEventSystemHandler, IEndDragHandler, IPointerDownHandler, IPointerClickHandler
{

	public Image sticker;
	public GameObject editStickerBorder;

	public LeanSelectable leanSelectable;
	public LeanDragTranslate dragTranslate;
	public LeanPinchScale pinchScale;
	public LeanTwistRotateAxis rotateAxis;

	public static List<StickerDrag> allStickerDrag = new List<StickerDrag>();

	//public static bool isDraggingSticker;

	public void SetImage(Sprite sprite)
    {
		sticker.sprite = sprite;
		sticker.preserveAspect = true;
		editStickerBorder.SetActive(true);
		allStickerDrag.Add(this);
		DisableAllSticker();
		OnPointerClick(null);
		//OnPointerDown(null);
		//MoveButton();
	}

	public void OnDrag(PointerEventData eventData)
	{
		
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (!UIManager.IsEditMode)
		{
			return;
		}

		editStickerBorder.SetActive(false);
		leanSelectable.IsSelected = false;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
        if (!UIManager.IsEditMode)
        {
			return;
        }

        if (leanSelectable.IsSelected)
        {
			editStickerBorder.SetActive(false);
			leanSelectable.IsSelected = false;
		}
        else
        {
			editStickerBorder.SetActive(true);
			leanSelectable.IsSelected = true;
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		/*editStickerBorder.SetActive(true);
		leanSelectable.IsSelected = true;*/
	}

	public void OnPointerUp(PointerEventData eventData)
	{
        /*if (rotateAxis.enabled || pinchScale.enabled)
        {
			OnEndDrag(null);
		}*/
	}

	public void DeleteButton()
    {
		allStickerDrag.Remove(this);
		Destroy(gameObject);
    }

	public void ScaleButton()
    {
		_ShowAndroidToastMessage("Use two finger to Scale sticker");
		/*pinchScale.enabled = true;
		rotateAxis.enabled = false;
		dragTranslate.enabled = false;*/
	}

	public void MoveButton()
	{
		/*dragTranslate.enabled = true;
		rotateAxis.enabled = false;
		pinchScale.enabled = false;*/
	}

	public void RotationButton()
    {
		_ShowAndroidToastMessage("Use two finger to Rotate sticker");
		/*rotateAxis.enabled = true;
		pinchScale.enabled = false;
		dragTranslate.enabled = false;*/
	}

	public void Info_button()
    {
		_ShowAndroidToastMessage("Use two finger to adjust sticker");
	}

	public static void DisableAllSticker()
    {
        foreach (var item in allStickerDrag)
        {
			item.editStickerBorder.SetActive(false);
			item.leanSelectable.IsSelected = false;
		}
    }

	// <param name="message">Message string to show in the toast.</param>
	private void _ShowAndroidToastMessage(string message)
	{
		AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

		if (unityActivity != null)
		{
			AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
			unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
			{
				AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity, message, 0);
				toastObject.Call("show");
			}));
		}
	}
}
