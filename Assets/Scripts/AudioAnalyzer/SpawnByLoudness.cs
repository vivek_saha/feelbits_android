using UnityEngine;

namespace AudioAnalyzer
{
	public class SpawnByLoudness : MonoBehaviour
	{
		public GameObject audioInputObject;

		public float threshold;

		public GameObject objectToSpawn;

		public float timeBetweenSpawns;

		private float timeSinceLastSpawn;

		public bool loudnessAffectsScale;

		public float scaleModifier;

		public bool randomRotationAlongY;

		public bool affectColor;

		public Color materialColor;

		private AudioAnalyzer audioIn;

		private void Start()
		{
		}

		private void Update()
		{
		}
	}
}
