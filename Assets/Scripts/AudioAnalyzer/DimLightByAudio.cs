using UnityEngine;

namespace AudioAnalyzer
{
	public class DimLightByAudio : MonoBehaviour
	{
		private AudioAnalyzer audioIn;

		public GameObject audioInputObject;

		public bool useFreqRange;

		public float intensityModifier;

		public float bandMinFreq;

		public float bandMaxFreq;

		public float absoluteMaxFreq;

		public float threshold;

		public float rDamp;

		private void Start()
		{
		}

		private void Update()
		{
		}
	}
}
