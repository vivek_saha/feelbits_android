using UnityEngine;

namespace AudioAnalyzer
{
	public class SpawnByFrequencyRange : MonoBehaviour
	{
		public GameObject audioInputObject;

		public GameObject objectToSpawn;

		public float threshold;

		public float timeBetweenSpawns;

		public float bandMinFreq;

		public float bandMaxFreq;

		public float absoluteMaxFreq;

		public bool loudnessAffectsScale;

		public float scaleModifier;

		public bool randomRotationAlongY;

		public bool affectColor;

		public Color materialColor;

		private float timeSinceLastSpawn;

		private AudioAnalyzer audioIn;

		private void Start()
		{
		}

		private void Update()
		{
		}
	}
}
