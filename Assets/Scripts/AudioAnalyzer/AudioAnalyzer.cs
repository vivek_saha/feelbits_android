using UnityEngine;

namespace AudioAnalyzer
{
	public class AudioAnalyzer : MonoBehaviour
	{
		private float fFreq;

		private float fLoud;

		private float fFreqLoud;

		private float[] fftData;

		public int cCount;

		public int bCount;

		private AudioSource audioSource;

		private AudioClip micClip;

		public int FFTBins;

		public int samplerate;

		public int averagingPeriod;

		public float sensitivity;

		public int inputDeviceNumber;

		public bool usesMicrophone;

		public float loudness
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		public float fundamentalFrequency
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		public float fundamentalFrequencyLoudness
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		public float[] frequencyData
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		private void Start()
		{
		}

		private void Update()
		{
		}

		private float GetAveragedVolume()
		{
			return 0f;
		}

		private float GetFundamentalFrequency()
		{
			return 0f;
		}

		private float ProcessData(float[] data)
		{
			return 0f;
		}
	}
}
