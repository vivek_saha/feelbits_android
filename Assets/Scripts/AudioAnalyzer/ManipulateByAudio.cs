using UnityEngine;

namespace AudioAnalyzer
{
	public class ManipulateByAudio : MonoBehaviour
	{
		private AudioAnalyzer audioIn;

		public GameObject audioInputObject;

		public bool affectTransform;

		public bool affectRotation;

		public bool affectScale;

		public bool useFreqRange;

		public bool scaleDecay;

		public Vector3 transformModifier;

		public Vector3 rotationAngleModifier;

		public Vector3 scaleModifier;

		public float scaleLimitMax;

		public float bandMinFreq;

		public float bandMaxFreq;

		public float absoluteMaxFreq;

		public float threshold;

		public float rDamp;

		public float sDamp;

		public float pDamp;

		private float freqsum;

		private float lastUpdate;

		private Vector3 startScale;

		private Vector3 startPos;

		private void Start()
		{
		}

		private void FixedUpdate()
		{
		}
	}
}
