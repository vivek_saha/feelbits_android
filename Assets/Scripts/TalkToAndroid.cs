using UnityEngine;

public class TalkToAndroid : MonoBehaviour
{
    public static TalkToAndroid I;

    public string tempImagePath;
    public string tempMp3Path;
    public string tempFramePath;
    public string tempOverlayPath;
    public string tempStickerPath;
    public int tempEffectValue;
    public string tempwatermark;
    public string temppath;

    [Space(30)]
    public EffectCount[] effectStart;

    private void Awake()
    {
        I = this;
    }

    private void Start()
    {
        //ApplyImageAndMusic(tempImagePath + "," + tempMp3Path);
        //ApplyFrame(tempFramePath);
        //ApplyOverlay(tempOverlayPath);
        //ApplyTheme(Random.Range(0, 6).ToString());
        //ApplyEffect(tempEffectValue.ToString());
        //ApplyMusic(tempMp3Path);
        //ApplyImage(tempImagePath);
        //ApplySticker(tempStickerPath);
        //CreateVideo(tempwatermark);
        //ShowSplashScreen();
        //FinishVideo(temppath);
        //ApplySticker(tempStickerPath);
        //ApplySticker(tempStickerPath);
    }

    public void ShowSplashScreen()
    {
        Debug.Log("In...ShowSplashScreen...");
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.sahafeel.bitsong.BridgeOfUnity");
        pluginClass.CallStatic("ShowSplashScreen", false);
    }

    public void FinishVideo(string path)
    {
        Debug.Log("In...FinishVideo...Index:" + path);
        AndroidJavaClass pluginClass = new AndroidJavaClass("com.sahafeel.bitsong.BridgeOfUnity");
        pluginClass.CallStatic("FinishVideo", path);
    }

    private void ApplyImageAndMusic(string imgMp3Path)
    {
        Debug.Log("In...ApplyImageAndMusic...Index:" + imgMp3Path);
        string imgPath = imgMp3Path.Split(',')[0];
        string mp3Path = imgMp3Path.Split(',')[1];
        UIManager.I.SetImageAndMusic(imgPath, mp3Path, true);
    }

    private void ApplyImage(string imgPath)
    {
        Debug.Log("In...ApplyImage...Index:" + imgPath);
        UIManager.I.SetImageAndMusic(imgPath, "", false);
    }

	private void ApplyMusic(string mp3Path)
	{
        Debug.Log("In...ApplyMusic...Index:" + mp3Path);
        UIManager.I.SetImageAndMusic("", mp3Path, false);
    }

    private void ApplyEffect(string effectIndex)
    {
        Debug.Log("In...ApplyEffect...Index:" + effectIndex);
        int index = int.Parse(effectIndex);
        UIManager.I.SetEffect(index);
    }

	private void ApplyTheme(string themeValue)
	{
        Debug.Log("In...ApplyTheme...Index:" + themeValue);
        int index = int.Parse(themeValue);
        UIManager.I.SetTheme(index);
    }

    private void ApplyFrame(string framePath)
    {
        Debug.Log("In...ApplyFrame...Index:" + framePath);
        if (string.IsNullOrEmpty(framePath))
        {
            UIManager.I.genericFrame.SetActive(false);
        }
        else
        {
            UIManager.I.SetFrame(framePath);
        }
    }

    private void ApplyOverlay(string overlayPath)
    {
        Debug.Log("In...ApplyOverlay...Index:" + overlayPath);
        if (string.IsNullOrEmpty(overlayPath))
        {
            UIManager.I.genericOverlay.SetActive(false);
        }
        else
        {
            UIManager.I.SetOverlay(overlayPath);
        }
    }

    private void CreateVideo(string watermarkFlag)
    {
        Debug.Log("In...CreateVideo...Index:" + watermarkFlag);
        UIManager.I.CreateAndSaveVideo(watermarkFlag == "1" ? true : false);
    }

    private void ApplySticker(string stickerPath)
    {
        Debug.Log("In...ApplySticker...Index:" + stickerPath);
        UIManager.I.SetSticker(stickerPath);
    }
}
