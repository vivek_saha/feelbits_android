using System.Collections.Generic;
using UnityEngine;

public class SimpleEffects : MonoBehaviour
{
	public List<ParticleSystem> particles;

	public List<Animator> beatAnimationObj;


    private void LateUpdate()
    {
        if (UIManager.AveragedVolume != 0)
        {
            foreach (var item in particles)
            {
                var main = item.main;
                main.simulationSpeed = UIManager.AveragedVolume / 25;
            }
        }
    }
}
